HOW TO SETUP

Install virtualenv, and create a virtual env
```shell
$ virtualenv --version
16.0.0
$ ls -l /usr/bin/python
lrwxrwxrwx 1 root root 7 Jan 11 00:51 /usr/bin/python -> python3
$ virtualenv venv -p /usr/bin/python3
$ source venv/bin/activate
(venv) $ python --version
Python 3.7.2
```


### Mail sender

in order to use the email sender, you'll need to create a `sender` object.

```python
from email import Sender

sender = input("your email address: ")
password = getpass()
receiver = input("receiver address: ")
s = Sender(sender, receiver, password)
s.send_mail(s.create_message("Hello, world"))
```


### Configuration

In order to save email and password from the user, you can use `Configuration` from `configuration.py`

```python
from configuration import Configuration


c = Configuration("/path/to/database.db")
c.create_table("user")
c.insert_configuration("user", "email@toto", "myp4ssw0rd")
```
