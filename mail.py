import smtplib
from getpass import getpass

class Sender(object):

    def __init__(self, 
            sender,
            receiver,
            password, 
            port=587, 
            server="smtp.gmail.com"):
        self.port = port
        self.server = server
        self.sender_email = sender
        self.receiver_email = receiver
        self.password = password

    def send_mail(self, msg):
        smtp_server = smtplib.SMTP(self.server, self.port)
        smtp_server.ehlo()
        smtp_server.starttls()
        smtp_server.login(self.sender_email, self.password)
        smtp_server.sendmail(self.sender_email, self.receiver_email, msg)
        smtp_server.close()


    def create_message(self, body):
        message = """\
                From: {sender}
                To: {to}
                Subject: calculator

                {body}""".format(sender=self.sender_email, to=self.receiver_email, body=body)
        return message
