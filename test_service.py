import pytest

from service import Service

@pytest.fixture
def ser():
    return Service()

def test_addition(ser):
    assert ser.addition(4, 1) == 5

def test_sub(ser):
    assert ser.soustraction(1, 5) == -4

def test_mul(ser):
    assert ser.multiplication(2, -4) == -8
