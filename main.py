from service import Service
from mail import Sender

from getpass import getpass

calc = Service()


def send_mail(msg):
    sender = input("your email address: ")
    pwd = getpass()
    receiver = input("receiver address: ")
    s = Sender(sender, receiver, pwd)
    s.send_mail(s.create_message(msg))




var = input("Do you want to calculate ? n/Y ")
if var=='Y':
    var = input("Do you want addition or substraction ? A/S: ")
    if var == 'A' : 
        a = input("Enter a first number: ")
        b = input("Enter a second number: ")

        res = calc.addition(int(a), int(b))
        content = f"{a} + {b} = {res}"
        print(content)
        send_mail(content)
        
    elif var == 'S' : 
        a = input("Enter a first number: ")
        b = input("Enter a second number: ")
        res = calc.soustraction(int(a), int(b))
        content = f"{a} - {b} = {res}"
        print(content)
        send_mail(content)
    
    else: 
        print("Unexpected input")
else:
    print("Bye")

